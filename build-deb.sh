#!/bin/bash

PKG="$(basename $PWD)"
SOURCEVERSION="0.2"
DEBVERSION="1"
RELEASE=bionic

echo "Preparing ${PKG}_${SOURCEVERSION}-${DEBVERSION}"
echo PKG=$PKG
echo SOURCEVERSION=$SOURCEVERSION
echo DEBVERSION=$DEBVERSION

if [[ -z DEBEMAIL ]] ; then
  DEBEMAIL="mark@trickkiste.at"
fi

if [[ -z DEBFULLNAME ]] ; then
  DEBFULLNAME="Markus Kienast"
fi

awk \
-v package="$PKG" \
-v debversion="(${SOURCEVERSION}-${DEBVERSION})" \
-v release="$RELEASE;" \
'NR==1{$1=package;$2=debversion;$3=release}1' debian/changelog > debian/changelog.$RELEASE
cp debian/changelog.$RELEASE debian/changelog
dh_make --defaultless --createorig -s -y -p ${PKG}_${SOURCEVERSION}
debuild -S
dput ppa:trickkiste/ceph-initramfs ../${PKG}_${SOURCEVERSION}-${DEBVERSION}_source.changes
#dpkg-buildpackage -rfakeroot -us -uc --build=source,all
#echo debsign xxx.changes
